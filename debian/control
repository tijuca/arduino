Source: arduino
Section: electronics
Priority: optional
Maintainer: Debian Electronics Team <pkg-electronics-devel@lists.alioth.debian.org>
Uploaders:
 Philip Hands <phil@hands.com>,
 Scott Howard <showard@debian.org>,
Build-Depends:
 ant,
 debhelper-compat (= 13),
 default-jdk,
 javahelper,
 libastyle-dev,
 libbatik-java,
 libbcpg-java,
 libbcprov-java,
 libcommons-beanutils-java,
 libcommons-codec-java,
 libcommons-compress-java,
 libcommons-exec-java,
 libcommons-io-java,
 libcommons-httpclient-java,
 libcommons-lang3-java,
 libcommons-logging-java,
 libcommons-net-java,
 libjackson2-annotations-java,
 libjackson2-core-java,
 libjackson2-databind-java,
 libjmdns-java,
 libjna-java,
 libjsch-java,
 libjssc-java,
 liblog4j2-java,
 librsyntaxtextarea-java,
 librxtx-java,
 libsemver-java,
 libtwelvemonkeys-java,
 lmodern,
 pandoc,
 texlive-latex-base,
 texlive-latex-recommended,
 unzip,
Standards-Version: 4.5.1
Homepage: https://www.arduino.cc
Vcs-Browser: https://salsa.debian.org/electronics-team/arduino/arduino
Vcs-Git: https://salsa.debian.org/electronics-team/arduino/arduino.git

Package: arduino
Architecture: any
Depends:
 arduino-builder,
 arduino-core-avr,
 avrdude,
 default-jre | openjdk-11-jre,
 dpkg-dev,
 libastylej-jni,
 libbatik-java,
 libbcpg-java,
 libbcprov-java,
 libcommons-codec-java,
 libcommons-compress-java,
 libcommons-exec-java,
 libcommons-io-java,
 libcommons-lang3-java,
 libcommons-logging-java,
 libcommons-net-java,
 libhttpclient-java,
 libjackson2-annotations-java,
 libjackson2-core-java,
 libjackson2-databind-java,
 libjaxp1.3-java,
 libjmdns-java,
 libjna-java,
 libjna-platform-java,
 libjsch-java,
 libjssc-java,
 liblistserialsj-dev,
 liblog4j2-java,
 librsyntaxtextarea-java,
 librxtx-java,
 libsemver-java,
 libslf4j-java,
 libxml-commons-external-java,
 libxmlgraphics-commons-java,
 ${misc:Depends},
Recommends:
 extra-xdg-menus,
 policykit-1,
Breaks:
 arduino-core (<< 2:1.5.6.2+sdfsg2-3.1),
Replaces:
 arduino-core (<< 2:1.5.6.2+sdfsg2-3.1),
Provides:
 arduino-core,
Description: AVR development board IDE from Arduino CC
 Arduino is an open-source electronics prototyping platform based on
 flexible, easy-to-use hardware and software. It's intended for artists,
 designers, hobbyists, and anyone interested in creating interactive
 objects or environments.
 .
 This package will install the integrated development environment that
 allows for program writing, code verification, compiling, and uploading
 to the Arduino development board. Example code will also be installed.
 .
 Some base AVR libraries will be provided by the depending additional
 package arduino-core-avr. More libraries can be installed within the IDE
 itself by calling the libarary manager. This requires a working internet
 access.
